var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var db = require('./db/db');
var passport = require('passport')
var N = require('./nuve');
var appConfig = require('./config/config')
var io = require('socket.io');


var config = require('./config/' + appConfig.configName);
console.log(appConfig.initAddress);
N.API.init(config.nuve.superserviceID, config.nuve.superserviceKey, appConfig.initAddress);




var routes = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');
var licode = require('./routes/licode');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));

app.use(session({ secret: 'keyboard cat' }));

// Passport:
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);
app.use('/auth', auth);
app.use('/users', users);
app.use('/licode', licode);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});




/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            user: req.user
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    //res.render('roomList', {
    //    message: err.message,
    //    error: {},
    //    user: req.user
    //});
    res.redirect('/rooms/');
});



module.exports = app;
