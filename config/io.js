var io = require('socket.io');

module.exports = function(app, server) {
    io = io.listen(server);
    app.set('io', io);


    io.sockets.on('connection', function(socket) {
        console.log(socket, ' is connected');


        socket.on('disconnect', function(){
            console.log(socket, ' disconnected');
        })
    })


}