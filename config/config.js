var _ = require('underscore')
    ,env;
var fs = require('fs');
switch (process.env.NODE_ENV) {
    case 'production':
        env = 'production';
        break;
    default:
        env = 'development';
        break;
}

var config = {
    development: {
        options : {
            key: fs.readFileSync(__dirname + '/../cert/key.pem').toString(),
            cert: fs.readFileSync(__dirname + '/../cert/cert.pem').toString()
        },
        db: 'mongodb://localhost/s2c',
        configName: 'licode_config',
        initAddress: 'http://localhost:3000/'
    },
    production: {
        options : {
            key: fs.readFileSync(__dirname +'/../cert_production/key.pem').toString(),
            cert: fs.readFileSync(__dirname +'/../cert_production/cert.pem').toString()
        },
        db: 'mongodb://localhost/s2c',
        configName: 'licode_config_production',
        initAddress: 'http://188.227.203.59:3000/'
    }
};

module.exports = _.extend(config[env]);



