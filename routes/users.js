var express = require('express');
var router = express.Router()
, mongoose = require('mongoose')
, User = mongoose.model('User');

/* GET users listing. */
router.get('/', function(req, res) {
  //User.find()
});


/* Update user. */
router.post('/update', function(req, res) {
    console.log('req.body', req.body, 'req.user._id', req.user._id);
    var user = {};
    user.email = req.body.email;
    user.linkedIn = req.body.linkedIn;
    user.info = req.body.info;
    user.siteURL = req.body.siteURL;
    user.password = req.user.password || req.body.password;
    User.update({_id: req.user._id}, user, function(err, result){
        if(result)
            res.redirect('/user/' + req.user._id);
        else
            res.send({error: 'Some error with update'})
    } )

});

var setAdminPermissions = function(userID) {
    User.update({_id: userID}, {admin: true}, function(err, result) {
        if(err)
            res.send({error: 'There\'re problems with setting admin permissions.'})
        else
            res.status(200).send(result.admin);
    })
}


module.exports = router;
