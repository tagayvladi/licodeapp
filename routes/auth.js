var express = require('express')
    , router = express.Router()
    , passport = require('passport')
    , passHash = require('password-hash')
    , LocalStrategy = require('passport-local').Strategy
    , mongoose = require('mongoose')
    , User = mongoose.model('User');


router.get('/', function(req, res) {
    res.render('index', { title: 'Express', message:'', user: req.user });
});

router.get('/logout', function(req, res){
        delete req.session.rooms;
        req.logout()
        res.redirect('/');
    });

router.post('/login',
    passport.authenticate('local', { failureRedirect: '/loginfail' }),
    function(req, res) {
        req.session.rooms = [];
        res.redirect('/user/' + req.user._id);
    });


router.get('/register', function(req, res){
    res.render('register', { title: 'Express', message:'', user: req.user });
})

router.post('/register', function(req, res, next) {
    console.log('req.body', req.body)
    User.findOne({username: { $regex: new RegExp("^" + req.body.username.toLowerCase(), "i") }}, function (err, profile) {
        if (profile) {
            return res.send({error: 'User with this name exists.'});
        }
        User.create({
            username: req.body.username,
            password: passHash.generate(req.body.password),
            email: req.body.email,
            linkedIn: req.body.linkedIn,
            info: req.body.info,
            siteURL: req.body.siteURL
        }, function (err, profile) {
            req.login(profile, function(err) {
                if (err) {
                    console.log(err);
                }
                res.send({userID: profile._id});
            });
        })
    })
});


passport.use(new LocalStrategy(
    {passReqToCallback: true},
    function(req, username, password, done) {
        User.findOne({ username: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, {message: 'User does not exist'}); }
            var hashedPassword = user.password;
            if (!passHash.verify(password, hashedPassword)) {
                return done(null, false, { message: 'Invalid password' });
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});


passport.deserializeUser(function(id, done) {
    User.findById(id, function(err,user){
        err
            ? done(err)
            : done(null,user);
    });
});



User.findOne({username: { $regex: new RegExp("^" + 'admin', "i") }}, function (err, profile) {
    if(profile) return;
    User.create({
        username: 'admin',
        password: passHash.generate('licodeAdmin'),
        email: 'admin@gmail.com',
        linkedIn: '',
        info: 'test administrator account for this resource',
        siteURL: 'http://streamlivebv.com:3000',
        admin: true
    }, function (err, profile) {
        if(err) console.log('problems with creating admin account.')
    })
})

module.exports = router;