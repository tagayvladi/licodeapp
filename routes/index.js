var express = require('express');
var router = express.Router();
var N = require('../nuve');

var auth = function(req, res, next){
    if (!req.isAuthenticated())
        res.redirect('/login')
    else{
        next();
    }
};

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' , message: req.message, user: req.user});
});

router.get('/register', function(req, res) {
    res.render('register', { title: 'Express', message:'', user: req.user });
});

router.get('/rooms', function(req, res) {
    res.render('roomList', { title: 'licode', user: req.user });
});

router.get('/rooms/:roomID', function(req, res) {
    N.API.getRoom(req.params.roomID, function(rm) {
        if(!rm) res.send('error');

        var room = JSON.parse(rm);
        if(room.data.password){
            if (req.user && req.user.admin) {
                res.render('conference', {user: req.user, room: room});
                return;
            }
            if(req.session.rooms && req.session.rooms.indexOf(room._id) != -1) {
                delete room.data.password;
                res.render('conference', {user: req.user, room: room});
            }
            else res.render('roomEntry', {room: { _id: room._id, name: room.name }, error: ''  });
        }
        else {
            res.render('conference', {user: req.user, room: room});
        }

    })
});

router.post('/rooms/:roomID', function(req, res) {
   // console.log('rooms:', req.session.rooms);

    var pw = req.body.pw;
    var roomID = req.params.roomID;
    N.API.getRoom(roomID, function(rm) {
        var room = JSON.parse(rm);
        //console.log()
        if(pw == room.data.password) {
            if(req.session.rooms)
                req.session.rooms.push(roomID);
            delete room.data.password;
            res.render('conference', {user: req.user, room: room});
        }
        else res.render('roomEntry', {room: { _id: room._id, name: room.name }, error: 'Incorrect password.'  });
    })
})



router.get('/login', function(req, res) {
    res.render('login', { title: 'Express' , message: '', user: req.user });
});

router.get('/loginfail', function(req, res) {
    res.render('login', { title: 'Express' , message: 'Invalid user credentials. Please try again.', user: req.user });
});

/* GET user page. */
router.get('/user/:user_id', auth, function(req, res) {
    if(req.user._id == req.params.user_id)
        res.render('userpage', { title: 'Express', user: req.user });
    else {
        res.redirect('/rooms/');
    }
});

/* GET user page. */
router.get('/error', function(req, res) {
    res.render('error', { title: 'Express', user: req.user});
});




module.exports = router;
