var express = require('express');
var router = express.Router();
var N = require('../nuve');


// helpful function

var deleteRoom = function(id, callback) {
    var callback = callback || function(){};
    N.API.deleteRoom(id, function(result) {
        console.log('Result: ', result);
        callback();
    });

}

router.get('/getRooms', function(req, res) {
    "use strict";
    N.API.getRooms(function(rooms) {
        // prevent from getting passwords on the client-side
        var roomsArr = JSON.parse(rooms)
        if(!roomsArr.length) return res.send(rooms);
        roomsArr.forEach(function(rm) {
            if(rm.data && rm.data.password) {
                delete rm.data.password;
                rm.data.hasPassword = true;
            } else {
                if(rm.data && !rm.data.password)
                    rm.data.hasPassword = false;
            }
        } )
        res.send(JSON.stringify(roomsArr));
    });
});


router.get('/getRoom/:room', function(req, res) {
    N.API.getRoom(req.params.room, function(room) {
        if(!room)
            res.send('error');
        else
            res.send(room);
    })
});

router.post('/create', function(req, res) {
    var roomName = req.body.roomName;
    var newRoom = req.body;
    var data = {
        adminBW : newRoom.adminBW,
        userBW: newRoom.userBW,
        maxUsers: newRoom.maxUsers,
        chat: newRoom.chat,
        muteControl: newRoom.muteControl,
        description: newRoom.roomDescription,
        password:newRoom.roomPassword,
        moderator: newRoom.moderator
    };

    N.API.getRooms(function(rooms) {
        if(rooms.indexOf(roomName) != -1) {
            res.send('denied');
            return;
        }
        N.API.createRoom(roomName, function(roomID) {
            res.send(roomID);
            console.log('created room' + roomID);
        }, function(e){
            console.log(e);
        }, {p2p: false, data: data })

    })
});


router.get('/getUsers/:room', function(req, res) {
    "use strict";
    var room = req.params.room;
    N.API.getUsers(room, function(users) {
        res.send(users);
    });
});


router.post('/createToken', function(req, res) {
    // "use strict";
    var room = req.body.room,
        username = req.body.username,
        role = req.body.role;
    N.API.createToken(room, username, role, function(token) {
        console.log(token + ' FOR ROOM # ' + room);
        res.send(token);

    });
});

router.delete('/:roomID', function(req, res) {
    var roomID = req.params.roomID;
    N.API.deleteRoom(roomID, function(result) {
        console.log('Result: ', result);
    }, function(e) {console.log(e)});
})


module.exports = router;
