# Video Conference Application based on Licode #

This is a guide how to install it and make it work. It requires virtual server for the Licode server with `node` and `npm` already installed, and also web server for this application.

##**Installation **##

*  First, you need to [install Licode](http://lynckia.com/licode/install.html) to the virtual server. 

*  Then, install `forever`. (on virtual server and web server too)
```
npm install forever -g
```
*  Run 
```
./licode/scripts/installBasicExample.sh
```
*  On web server, clone this repository: 

```
#!shell

git clone https://tagayvladi@bitbucket.org/tagayvladi/licodeapp.git
```


*  When Licode will be compiled and installed correctly, it needs to get some files. That files are being generated with random numbers for establishing secure connection, and they are required in our application. So, get them and replace with appropriate files.

These files are(on Licode virtual server): 

1. `./licode/extras/basic_example/cert` folder
2. `./licode/licode_config.js`
Place this files onto application folder the following way:
1. files from `cert` directory should be placed in `./licodeapp/cert_production` directory
2. `licode_config.js` to `./licodeapp/config/licode_config_production.js`

* Some scripts from initial version of Licode should be replaced with files that located in `licodeapp` repository, because we have to use `forever` to run Licode infinitely.

These scripts are located in `./licodeapp/other` folder. Copy them from licodeapp and put it into Licode server:

1. `initNuve.sh` -> `./licode/nuve/initNuve.sh`
2. `initErizo_agent.sh` -> `./licode/erizo_controller/initErizo_agent.sh`
3.  `initErizo_controller.sh` -> `./licode/erizo_controller/initErizo_controller.sh`

* Run Licode server:
```
./licode/scripts/initLicode.sh
```

* On web server, in `licodeapp` folder

```
npm install
bower install
```
And also install [MongoDB](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/).

* Run the application in production mode.

```
export NODE_ENV=production & node ./bin/www
```