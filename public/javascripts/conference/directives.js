var directives = angular.module('directives', []);

directives.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

directives.directive('chatBody', function () {
    return {
        restrict: 'E',
        replace: true,
        link: function(scope, el, attrs) {
            scope.$watchCollection('chat.chatMessages', function($scope) {
                // calculating, should we scroll chat down or not
                var pHeight = el.find('p:last').height();

                if(Math.abs(el[0].scrollHeight - el.height() - el[0].scrollTop - pHeight) < 50)
                    el[0].scrollTop = el[0].scrollHeight;
            })
        },
        template: '<div class="chatBody"><p ng-repeat="msg in chat.chatMessages">' +
        ' <b>{{msg.name}}:</b>' +
        '<span> {{msg.msg}}</span>' +
        '</p> </div>'
    }

});