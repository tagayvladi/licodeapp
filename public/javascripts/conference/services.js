var services = angular.module('services', ['ngResource']);

services.factory('API', ['$http',
    function ($http) {
        var doRequest = function (path, data, method, params) {
            if (!path) return;
            var method = method || 'GET'
                , data = data || {}
                , path = path
                , params = params || null;
            return $http({
                method: method,
                url: path,
                params: params,
                data: data
            })
        };
        return {
            request: function(path, data, method, params){
                return doRequest.apply(this, arguments);
            }
        }
    }
]);

