var conferenceApp = angular.module('conferenceApp', ['services', 'directives', 'ui.bootstrap']);

conferenceApp.run(function($rootScope, API) {
    $rootScope.makeID = function() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 7);
    };
    $rootScope.APIErrorHandler = function(d, s) {
        console.log(d, s);
    };

    $rootScope.createToken = function(username, role, room, callback) {
        var obj = {
            username: username,
            role: role,
            room: room
        };
        API.request('/licode/createToken', obj, 'POST').success(function(d, s) {
            if(d == 'error') return;
            $rootScope.token = d;
            callback()
        }).error($rootScope.APIErrorHandler);
    };

    $rootScope.getToken = function(roomID, callback) {
        var role = window.user.name ? 'presenter' : 'viewer';
        if(!$rootScope.token) {
            if(!window.user._id)
                var user = 'viewer' + $rootScope.makeID();
            else user = window.user.name;
            $rootScope.createToken(user, role, roomID, callback);
        }
        else
            callback();
    }
});

conferenceApp.controller('ConferenceCtrl',  ['$scope', '$rootScope', 'API',
        function($scope, $rootScope,  API){

            var localStream;
            $scope.streams = [];
            $scope.user = window.user;
            $scope.currentRoom = window.room;

            var roomOptions = {
                MAXUSERS    : $scope.currentRoom.data.maxUsers,
                ADMINBW     : $scope.currentRoom.data.adminBW,
                USERBW      : $scope.currentRoom.data.userBW,
                CHAT        : $scope.currentRoom.data.chat,
                MUTECONTROL : $scope.currentRoom.data.muteControl,
                roomID      : $scope.currentRoom._id,
                MODERATOR   : $scope.currentRoom.data.moderator
            };

            var userOptions = {
                canChat         : false,
                isAdmin         : $scope.user.isAdmin == 'true',
                enableChat      : false,
                hasPrivileges   : $scope.user.isAdmin == 'true' || roomOptions.MODERATOR == $scope.user._id
            };

            $scope.userOptions = userOptions;
            $scope.roomOptions = roomOptions;

            $scope.handleReceivedData = function(evt) {
                if(evt.msg.type === 'message') {
                    chat.chatMessageReceived(evt);
                    return;
                }
                if(evt.msg.type === 'command') {
                    if(!$scope.roomOptions.MUTECONTROL) return;
                    switch(evt.msg.action) {
                        case 'request':
                            $scope.$apply(function() {
                                $scope.muteControl.setStreamStatus('requested', evt.msg.id);
                            });
                            break;
                        case 'mute':
                            if(evt.msg.id === $scope.user._id) {
                                $scope.muteControl.muteMyself();
                            }
                            $scope.$apply(function() {
                                $scope.muteControl.setStreamStatus('muted', evt.msg.id);
                            });
                            break;
                        case 'unmute':
                            if(evt.msg.id === $scope.user._id) {
                                $scope.muteControl.unMuteMyself();
                            }
                            $scope.$apply(function() {
                                $scope.muteControl.setStreamStatus('speaks', evt.msg.id);
                            });
                            break;
                        default:
                            break

                    }
                }
            };

            //MUTECONTROL OPTIONS

            var muteControl = {
                requestToSpeak : function() {
                    localStream.sendData({type: 'command', action: 'request', id: $scope.user._id});
                    var attrs = localStream.getAttributes();
                    attrs.streamStatus = 'requested';
                    localStream.setAttributes(attrs);
                },
                mute : function(id) {
                    if(!$scope.userOptions.hasPrivileges) return;
                    localStream.sendData({type: 'command', action: 'mute', muter: $scope.user._id, id: id});
                    $scope.muteControl.setStreamStatus('muted', id);
                },
                unMute : function(id) {
                    if(!$scope.userOptions.hasPrivileges) return;
                    localStream.sendData({type: 'command', action: 'unmute', unMuter: $scope.user._id, id: id});
                    $scope.muteControl.setStreamStatus('speaks', id);

                },
                muteMyself : function() {
                    localStream.stream.getAudioTracks()[0].enabled = false;
                    var attrs = localStream.getAttributes();
                    attrs.streamStatus = 'muted';
                    localStream.setAttributes(attrs);
                    this.enableRequestButton();

                },
                unMuteMyself : function() {
                    localStream.stream.getAudioTracks()[0].enabled = true;
                    var attrs = localStream.getAttributes();
                    attrs.streamStatus = 'speaks';
                    localStream.setAttributes(attrs);
                    this.disableRequestButton();
                },
                setMute: function(stream) {
                    if(!$scope.userOptions.hasPrivileges) return;
                    if(stream.streamStatus == 'speaks')
                    {
                        this.mute(stream.customData._id);
                    }
                    else this.unMute(stream.customData._id);

                },
                setStreamStatus: function(status, id) {
                    for(var i = 0; i < $scope.streams.length; i++) {
                        if($scope.streams[i].customData._id === id) {

                            $scope.streams[i].streamStatus = status;
                            //})
                            return;
                        }
                    }
                },
                enableRequestButton: function() {
                    this.isButtonEnabled = true;
                },
                disableRequestButton: function() {
                    this.isButtonEnabled = false;
                },
                isButtonEnabled: true
            };

            //chat functions
            var chat = {
                chatMessages: [],
                enterPressed: function() {
                    this.sendChatMessage(localStream);
                },
                addTextToChat: function(name, msg) {
                    this.chatMessages.push({
                        name: name,
                        msg: msg
                    });
                },
                sendChatMessage : function(localStream) {
                    if($scope.msgText.match (/\S/)) {
                        localStream.sendData({msg: $scope.msgText.trim(), name: $scope.user.name, type: 'message'});
                        this.addTextToChat($scope.user.name, $scope.msgText);
                    }
                    $scope.msgText = '';
                },
                sendSystemMessage : function(localStream, name, message) {
                    localStream.sendData({msg: message, name: name, type: 'message'});
                    chat.addTextToChat(name, message);
                },
                chatMessageReceived : function(evt) {
                    var msg = evt.msg;
                    $scope.$apply(function() {
                        chat.addTextToChat(msg.name,msg.msg);
                    })
                }
            };

            $scope.muteControl = muteControl;
            if($scope.roomOptions.CHAT) {
                $scope.chat = chat;
            }

            $scope.init = function() {
                // needful functions for initialize the conference.

                $scope.subscribeToStreams = function (streams) {
                    for (var index in streams) {
                        var stream = streams[index];
                        if (localStream.getID() !== stream.getID()) {
                            room.subscribe(stream);
                        }
                    }
                };

                var room = Erizo.Room({token: $rootScope.token});
                room.connect();
                // event listeners

                room.addEventListener("room-connected", function (roomEvent) {


                    // max users handling
                    var streamsCount = roomEvent.streams.length;

                    if($scope.user._id && streamsCount < $scope.roomOptions.MAXUSERS) {
                        localStream = Erizo.Stream({video: true, audio: true, data: true, attributes: $scope.user , videoSize: [320, 240, 640, 480]});
                        $scope.userOptions.canChat = true;
                    }
                    else {
                        localStream = Erizo.Stream({video: false, audio: false, data: false});
                    }

                    localStream.init();
                    $scope.subscribeToStreams(roomEvent.streams);

                    //eventListener for CAMERA ACCESS ACCEPTED
                    localStream.addEventListener('access-accepted', function(event) {

                        console.log("Access to webcam and microphone granted");
                        localStream.show('myVideo');

                        $scope.$apply(function() {
                            $scope.userOptions.enableChat = true;
                        });

                        var bandWidth = 0;
                        if($scope.roomOptions.MODERATOR == $scope.user._id)
                            bandWidth = $scope.roomOptions.ADMINBW;
                        else
                            bandWidth = Math.floor($scope.roomOptions.USERBW / ($scope.roomOptions.MAXUSERS - 1));
                        room.publish(localStream, {maxVideoBW: bandWidth, maxFrameRate: 30});

                    });
                    //eventListener for CAMERA ACCESS DENIED
                    localStream.addEventListener('access-denied', function(event) {
                        console.log("Access to webcam and microphone rejected");
                    });

                });

                room.addEventListener("room-disconnected", function (roomEvent) {
                    console.log('disconnected to the room');
                    chat.sendSystemMessage(localStream, $scope.user.name, 'disconnected from the room');
                    $scope.userOptions.enableChat = false;
                });

                room.addEventListener("stream-added", function (streamEvent) {
                    var streams = [];
                    streams.push(streamEvent.stream);
                    $scope.subscribeToStreams(streams);
                });

                room.addEventListener("stream-subscribed", function(streamEvent) {
                    var stream = streamEvent.stream;

                    var loadStream = function(streamOwner) {
                        stream.customData = {
                            name     : streamOwner.name,
                            linkedIn : streamOwner.linkedIn,
                            info     : streamOwner.info,
                            siteURL  : streamOwner.siteURL,
                            _id      : streamOwner._id,
                            isAdmin  : streamOwner.isAdmin
                        };

                        if($scope.roomOptions.MUTECONTROL) {
                            var isStreamerPrivileged = function(stream) {
                                return (stream.customData.isAdmin || stream.customData._id == $scope.roomOptions.MODERATOR);
                            };

                            var startStatus = isStreamerPrivileged(stream) ? 'speaks' : 'muted';
                            stream.streamStatus = streamOwner.streamStatus || startStatus;
                        }

                        $scope.$apply(function() {
                            $scope.streams.push(stream);
                        });
                        stream.show(stream.getID());
                        console.log('Subscribed!')
                    };

                    var streamUser = stream.getAttributes();
                    loadStream(streamUser);
                    stream.addEventListener('stream-data', $scope.handleReceivedData);

                });

                room.addEventListener('stream-removed', function(roomEvent) {
                    var streamID = roomEvent.stream.getID();
                    $scope.$apply(function() {
                        $scope.streams.forEach(function(stream) {
                            if(stream.getID() == streamID) {
                                $scope.streams.splice($scope.streams.indexOf(stream), 1);
                            }
                        })
                    })
                });

            };
            //Let's go!

            API.request('/licode/getUsers/'+$scope.roomOptions.roomID).success(function(d, s){
                if(d.error) return;
                var isConnectedAlready = false;
                d.forEach(function(user) {
                    if(user === null) return;
                    if( $scope.user.name && user.name == $scope.user.name.toLowerCase()) {
                        isConnectedAlready = true;
                    }
                });
                if(isConnectedAlready) {
                    location.href = '/';
                } else{
                    $rootScope.getToken($scope.roomOptions.roomID, $scope.init)
                }

            }).error($rootScope.APIErrorHandler)

        }]
);