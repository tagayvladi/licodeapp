$(document).ready(function(){
    var $errorLabel = $('#error');
    var getNewPassword = function() {
        return $('#password').val();
    };
    var $form = $('#enterForm');
    $form.submit(function(e){
        var pw = getNewPassword();
        if(!pw.length) {
            $errorLabel.html('Enter the password.');
            e.preventDefault();
        } else {
            $errorLabel.html('');
        }
    })
});