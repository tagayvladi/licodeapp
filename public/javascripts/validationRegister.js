$(document).ready(function() {
    // validate signup form on keyup and submit
    var validator = $("#registrForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 6
            },
            confirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            email: {
                email: true
            },
            linkedIn: {
                url: true
            },
            siteURL: {
                url: true
            }
        },
        messages: {
            username: {
                required: "Enter a username",
                minlength: jQuery.validator.format("Enter at least {0} characters")
            },
            password: {
                required: "Provide a password",
                minlength: jQuery.validator.format("Enter at least {0} characters")
            },
            confirmPassword: {
                required: "Repeat your password",
                minlength: jQuery.validator.format("Enter at least {0} characters"),
                equalTo: "Enter the same password as above"
            },
            email: {
                required: "Please enter a valid email address",
                minlength: "Please enter a valid email address",
                remote: jQuery.validator.format("{0} is already in use")
            },
            linkedIn: {
                url: "Please enter a valid url"
            },
            siteURL: {
                url: "Please enter a valid url"
            }
        },
        // the errorPlacement has to take the table layout into account
//        errorPlacement: function(error, element) {
//            console.log('error')
//        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function() {
            $.post("/auth/register", { username: $('#username').val(), password: $('#password').val(),email: $('#email').val(),linkedIn: $('#linkedIn').val(),info: $('#info').val(),siteURL: $('#siteURL').val() })
                .success(function(data){
                    console.log('data', data);
                    if(data.userID){
                        location.href='/user/' + data.userID
                    } else {
                        $('#errorRegister').text('User with this name exists')
                    }
                })
                .error(function(data){
                    console.log('data', data);
                    if(data.error){
                        $('#errorRegister').text('User with this name exists')
                    }
                })
        },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
            // set &nbsp; as text for IE
            //label.html("").addClass("checked");
        }
//        highlight: function(element, errorClass) {
//            $(element).parent().next().find("." + errorClass).removeClass("checked");
//        }
    });
});
