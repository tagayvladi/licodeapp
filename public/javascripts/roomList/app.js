var roomsApp = angular.module('roomsApp', [ 'services']);

roomsApp.run(function($rootScope, API) {
    $rootScope.isUser = true; // temporary value

    $rootScope.makeID = function() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
    }
    $rootScope.APIErrorHandler = function(d, s) {
        console.log(d, s);
    }

});

roomsApp.controller('RoomListCtrl',  ['$scope', '$rootScope', 'API',
        function($scope, $rootScope,  API) {
            $scope.rooms = [];
            $scope.newRoomName = "";
            $scope.adminBW = 500;
            $scope.userBW = 1000;
            $scope.numOfUsers = 5;
            $scope.muteControl = false;
            $scope.chat = true;

            $scope.createRoom = function() {
                var data = {
                    roomName: $scope.roomName,
                    roomDescription: $scope.roomDescription,
                    roomPassword: $scope.roomPassword,
                    adminBW: $scope.adminBW,
                    userBW : $scope.userBW,
                    maxUsers: $scope.numOfUsers,
                    muteControl: $scope.muteControl,
                    chat: $scope.chat,
                    moderator: window.user._id
                }

                API.request('/licode/create', data, 'POST').success(function(d, s){
                    if(d == 'denied')
                        console.log('Room is already exist.');
                    else {
                        console.log(d);
                        var roomID = d._id;
                        location.href = '/rooms/' + roomID;
                    }
                }).error($rootScope.APIErrorHandler)
            }

            $scope.getRooms = function() {
                API.request('/licode/getRooms').success(function(d, s) {
                    $scope.rooms = d;
                    console.log(d);
                }).error($rootScope.APIErrorHandler)
            }
            $scope.getRooms();
        }]
);