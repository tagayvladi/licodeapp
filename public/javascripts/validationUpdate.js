$(document).ready(function() {
    // validate signup form on keyup and submit
    var validator = $("#userForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 2
            },
            password: {
                minlength: 6
            },
            confirmPassword: {
                minlength: 6,
                equalTo: "#password"
            },
            email: {
                email: true
            },
            linkedIn: {
                url: true
            },
            siteURL: {
                url: true
            }
        },
        messages: {
            username: {
                required: "Enter a username",
                minlength: jQuery.validator.format("Enter at least {0} characters")
            },
            password: {
                minlength: jQuery.validator.format("Enter at least {0} characters")
            },
            confirmPassword: {
                minlength: jQuery.validator.format("Enter at least {0} characters"),
                equalTo: "Enter the same password as above"
            },
            email: {
                minlength: "Please enter a valid email address"
            },
            linkedIn: {
                url: "Please enter a valid url"
            },
            siteURL: {
                url: "Please enter a valid url"
            }
        },
        // the errorPlacement has to take the table layout into account
//        errorPlacement: function(error, element) {
//            console.log('error')
//        },
        // specifying a submitHandler prevents the default submit, good for the demo
        submitHandler: function() {
            $.post("/users/update", { username: $('#username').val(), password: $('#password').val(),email: $('#email').val(),linkedIn: $('#linkedIn').val(),info: $('#info').val(),siteURL: $('#siteURL').val() })
                .success(function(data){
                    if(data.userID){
                        location.href='/' + data.userID
                    } else {
                        $('#errorRegister').text(data.error)
                    }
                })
                .error(function(data){
                    if(data.error){
                        $('#errorRegister').text(data.error)
                    }
                })

        },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
            // set &nbsp; as text for IE
            //label.html("").addClass("checked");
        }
//        highlight: function(element, errorClass) {
//            $(element).parent().next().find("." + errorClass).removeClass("checked");
//        }
    });
});
