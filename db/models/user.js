var mongoose = require('mongoose')
    , supergoose = require('supergoose')


var user = new mongoose.Schema({
    username: {type: String}
    , password: {type: String}
    , email: {type: String}
    , linkedIn: {type: String}
    , info: {type: String}
    , siteURL: {type: String}
    , admin: {type: Boolean}
    , rooms: {type:  [ {type: String} ] }
})




user.plugin(supergoose, {});
mongoose.model('User', user)
